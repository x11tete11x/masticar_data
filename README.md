# masticar_data

Pequeños scripts para tomar data de https://datos.gob.ar/dataset y volcarlas en una DB Influx consumible desde Grafana, es un proyecto pensado para usarlo localmente para jugar con data publica, no es 'production ready'

## Config
```
#get_data/config.yaml

<id>:
  url: <url del csv>
  influxdb_url: <url de la DB Influx>
  influxdb_token: <auth token de la DB Influx>
  influxdb_org: <org de la DB Influx>
  influxdb_bucket: <Bucket de la DB Influx>
  timestamp: <Nombre de la columna que va a ser utilizada como timestamp del dato a ingresar>
  select_columns: # Parametro opcional, columnas a importar
    - 'nombre 1'
    - 'nombre 2'
```

## Deploy

```
docker-compose up -d
```

Las credenciales se encuentran en los `.env` correspondientes

Para ingresar a la DB Influx: `http://localhost:8086`

Para ingresar a Grafana: `http://localhost:3000`

NOTA: Grafana ya viene con el DataSource de Influx cargado


## Devel

```
cd get_data
virtualenv venv/py
source venv/py/bin/activate
pip install -r requirements.txt
```
