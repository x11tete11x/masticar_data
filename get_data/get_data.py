#!/usr/bin/env python
import yaml
import time
import logging
import multiprocessing
import tempfile
import pandas as pd
from influxdb_client import InfluxDBClient

from concurrent.futures import ThreadPoolExecutor
pd.options.mode.chained_assignment = None

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(threadName)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

def load_data(id, config):
    logging.info(f"ID: {id}\nConfig:\n{yaml.dump(config)}")
    with tempfile.TemporaryDirectory() as tmpdirname:
        logging.info(f"ID: {id} - created temporary directory, {tmpdirname}")
        df = pd.read_csv(config['url'])
        columnts_to_select=[]
        if config.get('select_columns',None) is None:
            column_names = list(df.columns.values)
            column_names.remove(config['timestamp'])
            columnts_to_select = [config['timestamp']]+column_names
        else:
            columnts_to_select = [config['timestamp']]+config['select_columns']
        df_sel = df[columnts_to_select]
        df_sel[config['timestamp']] = pd.to_datetime(df_sel[config['timestamp']],infer_datetime_format=True,errors = 'coerce')
        df_sel=df_sel[pd.notnull(df_sel[config['timestamp']])]
        df_sel.set_index(config['timestamp'], inplace = True)
        #df_sel.drop_duplicates(keep='last', inplace=True)
        logging.info(f"ID: {id} - Loading data to InfluxDB")
        client = InfluxDBClient(url=config['influxdb_url'], token=config['influxdb_token'], org=config['influxdb_org'])
        write_api = client.write_api()
        write_api.write(bucket=config['influxdb_bucket'], data_frame_measurement_name=id, record=df_sel)
        logging.info(f"ID: {id} - Finished Upload")

if __name__ == '__main__':
    config={}
    with open("config.yaml", "r") as stream:
        try:
            config=yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    max_threads=multiprocessing.cpu_count()*2
    logging.info(f"Config:\n{yaml.dump(config)}\nMax Threads:{max_threads}")

    while True:
        executor = ThreadPoolExecutor(max_workers=max_threads)
        for key,value in config.items():
            executor.submit(load_data, key, value)
        executor.shutdown()
        logging.info(f"Sleep 1h")
        time.sleep(3600)
    
        